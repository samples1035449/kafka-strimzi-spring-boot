# Kafka Strimzi Spring Boot

# Deploy Strimzi using installation files
Before deploying the Strimzi cluster operator, create a namespace called kafka:
```
kubectl create namespace kafka
```
Apply the Strimzi install files, including ClusterRoles, ClusterRoleBindings and some Custom Resource Definitions (CRDs). The CRDs define the schemas used for the custom resources (CRs, such as Kafka, KafkaTopic and so on) you will be using to manage Kafka clusters, topics and users.

```
kubectl create -f docs/strimzi.io -n kafka
```

## Create an Apache Kafka cluster
Create a new Kafka custom resource to get a small persistent Apache Kafka Cluster with one node for Apache Zookeeper and Apache Kafka:

Apply the `Kafka` Cluster CR file

```
kubectl apply -n kafka -f docs/kafka-persistent-single.yaml
```
or to expose externally
```
kubectl apply -n kafka -f docs/kafka-persistent-exposed.yaml
```

## Enble Minikube tunnel 
```
minikube tunnel
```


Getting External my-cluster-kafka-external-bootstrap IP Address

```
kubectl get services -n kafka
```

## Send and receive messages
With the cluster running, run a simple producer to send messages to a Kafka topic (the topic is automatically created):

```
kubectl -n kafka run kafka-producer -ti --image=quay.io/strimzi/kafka:0.40.0-kafka-3.7.0 --rm=true --restart=Never -- bin/kafka-console-producer.sh --bootstrap-server my-cluster-kafka-bootstrap:9092 --topic scm
```
Once everything is set up correctly, you’ll see a prompt where you can type in your messages:

```
If you don't see a command prompt, try pressing enter.

>Hello strimzi!
```
And to receive them in a different terminal, run:
```
kubectl -n kafka run kafka-consumer -ti --image=quay.io/strimzi/kafka:0.40.0-kafka-3.7.0 --rm=true --restart=Never -- bin/kafka-console-consumer.sh --bootstrap-server my-cluster-kafka-bootstrap:9092 --topic scm --from-beginning
```
If everything works as expected, you’ll be able to see the message you produced in the previous step:

If you don't see a command prompt, try pressing enter.
```
>Hello strimzi!
```
Enjoy your Apache Kafka cluster, running on Minikube!

# Deleting
## Deleting your Apache Kafka cluster
When you are finished with your Apache Kafka cluster, you can delete it by running:

```
kubectl -n kafka delete $(kubectl get strimzi -o name -n kafka)
```

This will remove all Strimzi custom resources, including the Apache Kafka cluster and any KafkaTopic custom resources but leave the Strimzi cluster operator running so that it can respond to new Kafka custom resources.

## Deleting the Strimzi cluster operator
When you want to fully remove the Strimzi cluster operator and associated definitions, you can run:
```
kubectl -n kafka delete -f docs/strimzi.io
```