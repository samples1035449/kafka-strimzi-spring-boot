package it.kafka_strimzi.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@EnableScheduling
@SpringBootApplication
public class ProducerApplication {
  
  @Autowired
  MessageProducerService producer;

  public static void main(String[] args) throws Exception {
    SpringApplication.run(ProducerApplication.class, args);
  }
  
  @Scheduled(fixedDelay = 10000)
  void sendMessages(){
    /*
     * Sending a Hello World message to default topic 
     * Must be received by both listeners with group foo
     * and bar with containerFactory fooKafkaListenerContainerFactory
     * and barKafkaListenerContainerFactory respectively.
     * It will also be received by the listener with
     * headersKafkaListenerContainerFactory as container factory.
     */
    producer.sendMessage("Hello, World!");

    /*
     * Sending message to a topic with 5 partitions,
     * each message to a different partition. But as per
     * listener configuration, only the messages from
     * partition 0 and 3 will be consumed.
     */
    for (int i = 0; i < 5; i++) {
      producer.sendMessageToPartition("Hello To Partitioned Topic!", i);
    }

    /*
     * Sending message to 'filtered' topic. As per listener
     * configuration,  all messages with char sequence
     * 'World' will be discarded.
     */
    producer.sendMessageToTopicFiltered("Hello World!");

    /*
     * Sending message to 'greeting' topic. This will send
     * and received a java object with the help of
     * greetingKafkaListenerContainerFactory.
     */
    producer.sendToTopicGreeting(new Greeting("Greetings", "World!"));
  }

}
